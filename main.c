#include <stdio.h>
#include "calc.h"

int main()
{
    int addResult = add(1, 2);
    int subResult = sub(1, 2);
    int mulResult = mul(1, 2);

    printf("Result add = %d \n", addResult);
    printf("Result sub = %d \n", subResult);
    printf("Result mul = %d \n", mulResult);

    return 0;
}
